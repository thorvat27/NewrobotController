#define OPTIMIZATION2
#include <webots/Supervisor.hpp>
#include <webots/Servo.hpp>
#include <webots/Robot.hpp>
#include <webots/Gyro.hpp>
#include <webots/Camera.hpp>
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include <iostream>
#include <cmath>
#include "Eigen/Dense"
#include "Eigen/Geometry"
#include <ctime>
#include <fstream>
#include <sys/time.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <stdlib.h>
#include "controller.hpp"
#include "robotSim.hpp"
#include "joystick.h"
#include "MLP.hpp"




#define TIME_STEP   4


using namespace std;
using namespace Eigen;
using namespace webots;
int joystick_numbers = 1;



// GLOBAL CONFIG
int IS_SIMULATION;
int IS_OPTIMIZATION;
int USE_JOYSTICK;
int SWIM;
int SPINE_COMPENSATION_WITH_FEEDBACK;
int USE_REFLEXES;
int USE_TAIL;
int USE_IMU;
int USED_MOTORS[30];
int AUTO_RUN, AUTO_RUN_STATE;
int LOG_DATA;
int JOYSTICK_RECORDING_REPLAYING;
int Ntrunk, Nlegs, Ntail, Nneck, NUM_MOTORS;

int readGlobalConfig();
//############################ MAIN PROGRAM - links the controller and robot classes ###########################################################

int main(int argc, char **argv)
{
	if(readGlobalConfig()==0){
        return 0;
    }

    cout<<"MAIN STARTS"<<endl;
    double Td = TIME_STEP/1000.;
    double table_p[30], table_t[30];



    //======================================================//

    RobotSim robotSim(TIME_STEP);
    cout<<"ROBOTSIM CREATED"<<endl;
    robotSim.InitIMU();
    

    Controller controller(Td);
    cout<<"CONTROLLER CREATED"<<endl;

    

    double t=0, t_tot=0, freq=0.5;
    int k=1;

    double t0, t1, t2;
    double tt0, tt1, tt2, dt;
    int numsteps=0;
    tt0=get_timestamp();
    tt1=get_timestamp();
    tt2=get_timestamp();


    VectorXd torques(30), position(30), force(12);
    MatrixXd JFL(3,4), JFR(3,4), JHL(3,4), JHR(3,4), tmp(3,3);
    double d_posture[30], d_torques[30], compassData[3], IMUdata[3], gpsDataF[3], gpsDataH[3], ts_data[12+12], markers[4];
    double fgird_rotMat[9];
    double FL_feet_gpos[3], FR_feet_gpos[3], HL_feet_gpos[3], HR_feet_gpos[3], globalCoM[3];
    int count=0;

    MatrixXd globalPoly(3,4);
    bool robotMoved = false;
//=============================  LOOP  =============================================
    while(robotSim.step(TIME_STEP) != -1) {
        




        dt=get_timestamp()-tt0;
        tt0=get_timestamp();
        dt=Td;
        t=t+dt;

        if(t>1.3 && !robotMoved){
            //double p[3]={-1, -0.1, -0.0657};  // walls
            double p[3]={-1, -0.1, -1.23292};  // pipe test
            //robotSim.setPositionOfRobot(p);

            robotMoved=true;
        }

        controller.setTimeStep(dt);
        //controller.getCoM().transpose();

        robotSim.getPositionTorques(d_posture, d_torques);
        robotSim.ReadIMUAttitude(fgird_rotMat);
        robotSim.ReadTouchSensors(ts_data);
        //robotSim.GetCompass(compassData);
        
        robotSim.GetPosition(gpsDataF, gpsDataH);
        robotSim.GetFeetGPS(FL_feet_gpos, FR_feet_gpos, HL_feet_gpos, HR_feet_gpos);

        

        controller.getAttitude(fgird_rotMat);
        controller.getGPS(gpsDataF, FL_feet_gpos, FR_feet_gpos, HL_feet_gpos, HR_feet_gpos);
        controller.updateRobotState(d_posture, d_torques);

        controller.getForce(ts_data);

        //controller.GetCompass(compassData);
        if(!controller.runStep()){
            break;
        }
        controller.getAngles(table_p);
        controller.getGlobalCoM(globalCoM);
        //globalPoly=controller.getGlobalSupportPoly();
            


        robotSim.setAngles(table_p, USED_MOTORS);
        //robotSim.setCoMmarker(globalCoM);
        //robotSim.setSupportPoly(globalPoly, controller.legs_stance, 0.001);




        controller.sendStatusToMatlab(5, "128.178.148.33");

        //===============================================================================================//
        
        tt1=get_timestamp ();
        //cout<<(tt1 - tt0) <<"s"<<"\t"<<(tt1 - tt0) * 1000<<"ms"<<"\t\t"<<endl;
        if(Td>(tt1-tt0)){
       //     usleep((Td- (tt1-tt0))*1000000 );
        }

        tt2=get_timestamp();

        if(count%3==0){
            //sendUDP(&vel, 2*sizeof(double), "128.178.148.59", 8472);
        }
        


    }

    //controller.mpc_thread.join();
    //controller.fdo_thread.join();

//==============================================================================
  return 0;
}




















// load global config
int readGlobalConfig()
{
    stringstream stringstream_file;
    ifstream global_config_file("config/GLOBAL.config");
    if(global_config_file.is_open()){
        readFileWithLineSkipping(global_config_file, stringstream_file);
        stringstream_file >> IS_SIMULATION;
        stringstream_file >> IS_OPTIMIZATION;
        stringstream_file >> USE_JOYSTICK;
        stringstream_file >> SWIM;
        stringstream_file >> SPINE_COMPENSATION_WITH_FEEDBACK;
        stringstream_file >> USE_REFLEXES;
        stringstream_file >> USE_TAIL;
        stringstream_file >> USE_IMU;
        stringstream_file >> AUTO_RUN;
        stringstream_file >> AUTO_RUN_STATE;
        stringstream_file >> LOG_DATA;
        stringstream_file >> JOYSTICK_RECORDING_REPLAYING;
        stringstream_file >> Nlegs;
        stringstream_file >> Ntrunk;
        stringstream_file >> Nneck;
        stringstream_file >> Ntail;
        NUM_MOTORS=4*Nlegs + Ntrunk + Nneck + Ntail;
        
        // IDs
        cout << "USED_IDS: \n";
        for(int i=0; i<NUM_MOTORS; i++){
            stringstream_file >> USED_MOTORS[i];
        }
        return 1;
    }
        
    else{
        return 0;
    }
}



